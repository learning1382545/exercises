# Crear el hijo colectivo que herede lo del padre Auto

class Auto:
    def __init__(self, nombre, max_vel, kilometraje):
        self.nombre = nombre
        self.max_vel = max_vel
        self.kilometraje = kilometraje


class Colectivo(Auto):
    pass


autonuevo = Colectivo("autito", 180, 12)
print("Nombre: ", autonuevo.nombre, " Velocidad: ", autonuevo.max_vel, " Kilometraje: ", autonuevo.kilometraje)
