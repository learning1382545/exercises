# Crear un atributo que se llame color y que por default sea blanco

class Auto:
    def __init__(self, nombre, max_vel, kilometraje, color="blanco"):
        self.nombre = nombre
        self.max_vel = max_vel
        self.kilometraje = kilometraje
        self.color = color


class Colectivo(Auto):
    pass


class autito(Auto):
    pass


colectivo_nuevo = Colectivo("bondi", 180, 12, "negro")
autito_nuevo = Auto("autito", 120, 6)
print(colectivo_nuevo.color)
print(autito_nuevo.color)
