# Determinar si Colectivo es una instancia de Auto

class Auto:
    def __init__(self, nombre, max_vel, kilometraje):
        self.nombre = nombre
        self.max_vel = max_vel
        self.kilometraje = kilometraje


class Colectivo(Auto):
    pass

    def calculo(self, tarifa):
        self.tarifa = tarifa


bondi = Colectivo("nombre", 40, 30)
bondi.calculo(40)

print(isinstance(bondi, Auto))
