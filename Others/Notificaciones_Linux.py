import gi

gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Test Notifier")

notification = Notify.Notification.new(
    "primera linea",
    "hola <b>en negrita</b>?"
)

notification.set_urgency(1)     #1 es advertencia - 2 es error (los simbolitos)
notification.show()
