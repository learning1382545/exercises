#!/usr/bin/python3
# La linea de aca arriba tiene que estar!

# Este programa corre con el ejemplo de Augusto gdbus_test

# Ir a la carpeta: .../Temporal/SmartHome/Augusto/gdbus_test/src/build
# Correr por consola el gdbus_test (este funciona como el servidor)
# Para chequear bus, object_path e interface, se puede
# Ver los programas de Augusto o tambien se puede observar desde el d-feet

# Todas las funciones que son usadas, las tiene que tener implementadas Augusto en su servidor

import dbus, glob, os

# Me traigo los parametros de bus, object e interface del archivo .pdf
os.chdir("/home/fcapdeville/Temporal/Smart Home/Augusto/gdbus_test/src/interfaces")
nombre = glob.glob("*pdf") # Busco el nombre del archivo
nombre = nombre[0].replace('out-', '')
nombre = nombre.replace('.pdf', '')

interface = nombre  # consigo el interface
nombre = nombre.split('.')
bus_name = nombre[0] + "." + nombre[1] + "." + "bus"    # consigo el bus_name
object_path = "/" + nombre[0] + "/" + nombre[1] + "/" + nombre[3]   # consigo el object_path

# Bus
bus = dbus.SessionBus()
# Object
the_object = bus.get_object(bus_name, object_path)
# Interface
the_interface = dbus.Interface(the_object, interface)

# Llamado a metodos
a_enviar = input("Ingrese el elemento a enviar: ")
reply = the_interface.PrintMessage(a_enviar)
print(reply)

# Metodo de fantasia
try:
    devuelve = the_interface.Fantasy(2, 2, 1)
    print(devuelve)
except:
    print("Falla la funcion de Fantasy")

print("Sigue el programa todo piola")