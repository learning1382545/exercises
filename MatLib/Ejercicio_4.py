# Leer las ventas de "toothpaste" y hacer un grafico de puntos

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthList = df['month_number'].to_list()
toothPasteSalesData = df['toothpaste'].to_list()

plt.scatter(monthList, toothPasteSalesData, label='Tooth paste Sales Data')

plt.xlabel('Month number')
plt.ylabel('Number of units sold')
plt.legend(loc='upper left')
plt.title('Tooth paste Sales Data')
plt.xticks(monthList)
plt.grid(True, linewidth=1, linestyle="--")
plt.show()
