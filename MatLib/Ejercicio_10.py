# Mostrar un "stack plot" (es como un grafico de curva, pero todo lo ques esta debajo va pintado)

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthlist = df['month_number'].to_list()

facecreamsalesdata = df['facecream'].to_list()
facewashsalesdata = df['facewash'].to_list()
toothpastesalesdata = df['bathingsoap'].to_list()
bathingsoapsalesdata = df['bathingsoap'].to_list()
shampoosalesdata = df['shampoo'].to_list()
moisturizersalesdata = df['moisturizer'].to_list()

plt.plot([], [], color='m', label='face cream', linewidth=5)
plt.plot([], [], color='c', label='face wash', linewidth=5)
plt.plot([], [], color='r', label='tooth paste', linewidth=5)
plt.plot([], [], color='k', label='bathing soap', linewidth=5)
plt.plot([], [], color='g', label='shampoo', linewidth=5)
plt.plot([], [], color='y', label='moisturizer', linewidth=5)

plt.stackplot(monthlist, facecreamsalesdata, facewashsalesdata, toothpastesalesdata,
              bathingsoapsalesdata, shampoosalesdata, moisturizersalesdata,
              colors=['m', 'c', 'r', 'k', 'g', 'y'])

plt.xlabel('month number')
plt.ylabel('Sales units in Number')
plt.title('All product sales data using stack plot')
plt.legend(loc='upper left')
plt.show()
