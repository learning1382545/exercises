# Mostrar en grafico con linea punteada

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
profilist = df['total_profit'].to_list()
monthList = df['month_number'].to_list()

plt.plot(monthList, profilist, label = 'Profit data of last year',
         color='r', marker='o', markerfacecolor='k',
         linestyle='--', linewidth=3)

plt.xlabel('Month number')
plt.ylabel('Profit in dollar')
plt.legend(loc='lower right')
plt.title('Company Sales data of the year')
plt.xticks(monthList)
plt.yticks([100000, 200000, 300000, 400000])
plt.show()
