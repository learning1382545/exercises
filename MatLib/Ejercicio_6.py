# Leer y hacer graficos de barras. Luego, guardarlo en el disco duro

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthList = df['month_number'].to_list()
bathingsoapSalesData = df['bathingsoap'].to_list()

plt.bar(monthList, bathingsoapSalesData)

plt.xlabel('Month number')
plt.ylabel('Sales units in number')
plt.title('Sales data')

plt.xticks(monthList)
plt.grid(True, linewidth=1, linestyle="--")
plt.title('Bathingsoap Sales Data')
plt.savefig('/home/fcapdeville/PycharmProjects/cosas_python/MatLib/ejercicio_6.png', dpi=150)
plt.show()
