# Hacer un grafico de torta

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthlist = df['month_number'].to_list()

labels = ['facecream', 'facewash', 'toothpaste', 'bathins soap', 'shampoo', 'moisturizer']
salesdata = [df['facecream'].sum(), df['facewash'].sum(), df['toothpaste'].sum(),
             df['bathingsoap'].sum(), df['shampoo'].sum(), df['moisturizer'].sum()]
plt.axis("equal")
plt.pie(salesdata, labels=labels, autopct='%1.1f%%')
plt.legend(loc='lower right')
plt.title('sales data')
plt.show()
