# Hacer un histograma

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
profitlist = df['total_profit'].to_list()
labels=['low', 'average', 'good', 'best']

profit_range = [150000, 175000, 200000, 225000, 250000, 300000, 350000]
plt.hist(profitlist, profit_range, label='profit data')

plt.xlabel('profit range in dollar')
plt.ylabel('actual profit in dollar')
plt.legend(loc='upper left')
plt.xticks(profit_range)
plt.title('profit data')
plt.show()
