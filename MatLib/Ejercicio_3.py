# Mostrar todos los productos y usar varias lineas

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthList = df['month_number'].to_list()
faceCremSalesData = df['facecream'].to_list()
faceWashSalesData = df['facewash'].to_list()
toothPasteSalesData = df['toothpaste'].to_list()
bathingsoapSalesData = df['bathingsoap'].to_list()
shampooSalesData = df['shampoo'].to_list()
moisturizerSalesData = df['moisturizer'].to_list()

plt.plot(monthList, faceCremSalesData, label='Face cream Sales Data', marker='o', linewidth=3)
plt.plot(monthList, faceWashSalesData, label='Face wash Sales Data', marker='o', linewidth=3)
plt.plot(monthList, toothPasteSalesData, label='Toothpaste Sales Data', marker='o', linewidth=3)
plt.plot(monthList, bathingsoapSalesData, label='Bathing Soap Sales Data', marker='o', linewidth=3)
plt.plot(monthList, shampooSalesData, label='Shampoo Sales Data', marker='o', linewidth=3)
plt.plot(monthList, moisturizerSalesData, label='Moisturizer Sales Data', marker='o', linewidth=3)

plt.xlabel('Month number')
plt.ylabel('Sales units in number')
plt.legend(loc='upper left')
plt.xticks(monthList)
plt.yticks([1000, 2000, 4000, 6000, 8000, 10000, 12000, 15000, 18000])
plt.title('Sales Data')
plt.show()
