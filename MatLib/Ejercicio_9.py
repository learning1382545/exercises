# Hacer un subplot (dos graficos en uno)

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthlist = df['month_number'].to_list()
bathingsoap = df['bathingsoap'].to_list()
facewashsalesdata = df['facewash'].to_list()

f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(monthlist, bathingsoap, label='Bathingsoap sales data', color='k', marker='o', linewidth=3)
axarr[0].set_title('sales data of a bathingsoap')
axarr[1].plot(monthlist, facewashsalesdata, label='Face wash sales data', color='r', marker='o', linewidth=3)
axarr[1].set_title('sales data of a facewash')

plt.xticks(monthlist)
plt.xlabel('month number')
plt.ylabel('sales units in number')
plt.show()
