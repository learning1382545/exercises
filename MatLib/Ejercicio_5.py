# Leer y hacer graficos de barras

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
monthList = df['month_number'].to_list()
facecremSalesData = df['facecream'].to_list()
facewashSalesData = df['facewash'].to_list()

plt.bar([a-0.25 for a in monthList], facecremSalesData, width=0.25, label='Face Cream Sales Data', align='edge')
plt.bar([a+0.25 for a in monthList], facecremSalesData, width=-0.25, label='Face Wash Sales Data', align='edge')

plt.xlabel('Month number')
plt.ylabel('Sales units in number')
plt.legend(loc='upper left')
plt.title('Sales data')

plt.xticks(monthList)
plt.grid(True, linewidth=1, linestyle="--")
plt.title('Facewash and facecream Sales Data')
plt.show()
