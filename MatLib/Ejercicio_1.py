# Leer de un csv y mostrarlo en un grafico

import os
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("MatLib/company_sales_data.csv")
profilist = df['total_profit'].to_list()
monthList = df['month_number'].to_list()
plt.plot(monthList, profilist, label = 'Month-wise Profit data of last year')
plt.xlabel('Month number')
plt.ylabel('Profit in dollar')
plt.xticks(monthList)
plt.title('Company profit per month')
plt.yticks([100000, 200000, 300000, 400000])
plt.show()
