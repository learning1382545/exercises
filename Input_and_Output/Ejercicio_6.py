# Escribir los datos de un archivo .txt en otro, salteandose una linea

with open("Ejercicios/Input_and_Output/Ej5primero.txt", "r") as fp :
    lineas = fp.readlines()         # Leo todas las lineas del archivo
    contador = len(lineas)

print("Tiene ", contador, " lineas")
print (lineas)

with open("Ejercicios/Input_and_Output/Ej5segundo.txt", "w") as fp :
    for line in lineas :
        if contador == 5 :
            contador -= 1
            continue
        else :
            fp.write(line)
        contador -= 1