# Convertir un decimal a un hexadecimal usando el print()

numero = 15

# Esto es en hexa
print("El numero es: ", '%0x, '%(numero))

# Esto es octal
print("El numero es: ", '%o, '%(numero))

# Esto es binario
print("El numero es: ", format(numero, '08b'))
