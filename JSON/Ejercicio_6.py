# Convertir el objeto a un JSON

import json
from json import JSONEncoder

class Vehicule:
    def __init__(self, name, engine, price):
        self.name = name
        self.engine = engine
        self.price = price


class VehiculeEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

vehicule = Vehicule("Toyota", "2.5L", 32000)

print("Transformo al JSON:")
vehiculeJSON = json.dumps(vehicule, indent=4, cls=VehiculeEncoder)
print(vehiculeJSON)
