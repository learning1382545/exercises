# Muestreo PrettyPrint de un JSON

import json

data = {"key1": "value1", "key2": "value2"}

pretty = json.dumps(data, indent=2, separators=(",", " = "))
print(pretty)
