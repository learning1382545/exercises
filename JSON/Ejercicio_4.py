# Elegir keys y escribirlas en un archivo

sampleJSON = {"id" : 1, "name" : "value2", "age" : 29}

import json

with open("/Ejercicios/JSON/sampleJSON.json", "w") as write_file:
    json.dump(sampleJSON, write_file, indent=4, sort_keys=True)
