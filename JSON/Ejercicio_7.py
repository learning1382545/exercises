# Convertir el JSON en un Vehicle object

import json

class Vehicule:
    def __init__(self, name, engine, price):
        self.name = name
        self.engine = engine
        self.price = price


def vehiculeDecoder(obj):
    return Vehicule(obj['name'], obj['engine'], obj['price'])

vehiculeObj = json.loads('{"name": "Toyota", "engine": "2.5L", "price": 32000}', object_hook=vehiculeDecoder)

print("Tipo de objeto decoded de JSON data")
print(type(vehiculeObj))
print("Datos del vehiculo")
print(vehiculeObj.name, vehiculeObj.engine, vehiculeObj.price)
