# Agregar una semana y 12 horas a la fecha dada

from datetime import datetime, timedelta

given_date = datetime(2020, 3, 22, 10, 0, 0)

print(given_date + timedelta(days=7, hours=12))
