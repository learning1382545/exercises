# Sustraer una semana a partir de una fecha dada

from datetime import datetime, timedelta

given_date = datetime(2020, 2, 25)

print(given_date - timedelta(days=7))
