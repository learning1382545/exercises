# Encontrar el dia de la semana de la fecha

from datetime import datetime

given_date = datetime(2020, 10, 26)

# sale como integer
print(given_date.weekday())

# sale como el nombre
print(given_date.strftime('%A'))
