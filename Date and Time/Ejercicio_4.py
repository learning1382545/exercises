# Mostrar una fecha en un formato especifico

from _datetime import datetime

given_date = datetime(2020, 2, 25)

print(given_date.strftime('%A %d %B %Y'))
