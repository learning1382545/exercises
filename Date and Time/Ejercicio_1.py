import datetime

# Muestra el tiempo
print(datetime.datetime.now())

# Muestra solo el tiempo
print(datetime.datetime.now().time())
