# Se ingresan dos numeros por teclado, devolver el producto
# Si el producto es mayor a 1000, devolver la suma

def funcion_1(numero_1, numero_2) :
    if (numero_1 * numero_2) > 1000:
        print("La multiplicacion es mayor a 1000, devuelvo la suma: ")
        print(numero_1 + numero_2)
    else:
        print("La multiplicacion es menor a 1000: ")
        print(numero_1 * numero_2)

numero_1 = input("Ingrese el primer numero: ")
numero_1 = int(numero_1)
numero_2 = input("Ingrese el segundo numero: ")
numero_2 = int(numero_2)

funcion_1(numero_1, numero_2)