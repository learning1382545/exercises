# Dada una lista de numeros, devuelve True si el primero y el ultimo son iguales

def funcion(lista1, lista2) :
    if (lista1[0] == lista2[0]) and (lista1[-1] == lista2[-1]) :
        return True;
    else :
        return False;


lista1 = [10, 20, 30, 40, 10]
lista2 = [10, 20, 30, 40, 50]
lista3 = [10,  5,  5,  5, 10]

booleano = funcion(lista1, lista2)

print ("La comparacion fue: ", booleano)