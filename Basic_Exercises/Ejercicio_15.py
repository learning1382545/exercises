# Escribir una funcion que regrese el valor de una exponencial base elevado a la exponente

def exponencial(base, exponente) :
    calculo = 1

    while(exponente > 0) :
        calculo = calculo * base
        exponente = exponente - 1
    print(calculo)


exponencial(2,5)
print(" ")
exponencial(5,4)