# Imprimir el siguiente patron

# 1
# 2 2
# 3 3 3
# 4 4 4 4
# 5 5 5 5 5

def patron(contador):
    for i in range(1, contador):
        for j in range(0, i):
            print(i, end=" ")
        print("\n")


patron(6)