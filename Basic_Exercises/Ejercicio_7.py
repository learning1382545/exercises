# Devolver la totalidad de veces que un sub-string aparece en un string mayor

def inclusive(stringchico, stringgrande) :
    contador = stringgrande.count(stringchico)
    return contador


string_chico = "Emma"
string_grande = "Emma is good developer. Emma is a writer"

contador = inclusive(string_chico, string_grande)

print("La cantidad de veces fueron: ", contador)