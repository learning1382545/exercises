# A partir de un integer, obtener un string de sus numeros invertidos y separados por un espacio

def inversion(numero) :
    final = ""
    inverso = numero

    while (inverso > 0) :
        final = final + str(inverso % 10) + " "
        inverso = inverso // 10
    return final

numero = 7536

invertido = inversion(numero)

print("El numero invertido es: ", invertido)