# Dado un string y un numero, remover los caracterres del string arrancando de cero hasta n y devolver el string que
# queda

# "pynative", 4 ==> queda "tive"

def ejercicio(palabra, numero) :
    return palabra[numero:]


ingreso = "pynative"
numero = 4

palabra = ejercicio(ingreso, numero)

print("Lo que queda de la palabra es: ", palabra)
