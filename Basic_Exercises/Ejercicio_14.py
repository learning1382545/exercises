# Mostrar un el siguiente parametro

# * * * * *
# * * * *
# * * *
# * *
# *

def piramide(cantidad) :
    for i in range(cantidad) :
        for j in range(cantidad-i) :
            print("* ", end= " ")
        print(" ")

piramide(5)