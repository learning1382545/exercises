# Indicar si un numero es capicua o no

def capicua (numero) :
    copia = 0
    original = numero

    while (original > 0) :
        copia = copia + original % 10
        original = original // 10
        if original != 0 :
            copia = copia * 10
    if copia == numero :
        print("Es capicua")
    else :
        print("No es capicua")


numero1 = 121
numero2 = 125

capicua(numero1)
capicua(numero2)