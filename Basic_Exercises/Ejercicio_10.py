# Dadas dos listas con numeros, crear una nueva que contenga los impares de la primera y los pares de la segunda

def lista_nueva(lista1, lista2) :
    lista_final = []

    for i in range(len(lista1)) :
        if (lista1[i] % 2) != 0 :
            lista_final.append(lista1[i])
    for i in range(len(lista2)) :
        if (lista2[i] % 2) == 0 :
            lista_final.append(lista2[i])
    return lista_final


lista1 = [10, 20, 23, 11, 17]
lista2 = [13, 43, 24, 36, 12]

lista_final = lista_nueva(lista1, lista2)
print("La lista final es: ", lista_final)