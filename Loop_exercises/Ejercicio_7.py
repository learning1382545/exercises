# Imprimir el siguiente patron

# 5 4 3 2 1
# 4 3 2 1
# 3 2 1
# 2 1
# 1

def piramide(numero) :
    for i in range(1,numero+1) :
        for j in range(1,numero+1) :
            if numero+2-i-j > 0 :
                print(numero+2-i-j, end= " ")
        print("")

piramide(5)