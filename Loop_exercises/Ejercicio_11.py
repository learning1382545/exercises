# Mostrar todos los numeros primos que hay entre un comienzo y fin indicado por parametros

def primos(comienzo, fin) :
    primos = 0

    for i in range (comienzo, fin) :
        for j in range (2, i) :
            if i % j == 0 :
                primos = primos + 1
        if primos == 0 :
            print (i)
        primos = 0
    return

comienzo = 25
fin = 50
primos(comienzo, fin)