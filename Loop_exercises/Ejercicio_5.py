# Dada una lista, mostrar los numeros divisibles por 5. Si se encuentra un numero mayor a 150 parar y salir

def funcion(lista) :
    for i in range (len(lista)) :
        if lista[i] <= 150 :
            if (lista[i] % 5) == 0 :
                print(lista[i])
        else :
            return

lista = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]
funcion(lista)