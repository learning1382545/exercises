# Retornar un numero invertido

def invertido(numero):
    inversion = 0

    while numero > 0:
        inversion = inversion + numero % 10
        numero = numero // 10
        if numero > 0 :
            inversion = inversion * 10
    print(inversion)


invertido(76542)
