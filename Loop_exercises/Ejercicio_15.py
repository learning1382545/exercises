# Mostrar las posiciones impares dentro de la lista

lista1 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]


def muestreo(lista):
    for i in range(0, len(lista)):
        if i % 2 != 0:
            print(lista[i])


muestreo(lista1)
